package com.roamtech.android.fourgolf.models;

/**
 * Created by dennis on 3/15/16.
 */
public class UserItems {
    public String imageLink;
    public String name;
    public String email;
    public String balance;
    public String wins;
    public String handiCap;
    public String rounds;

    public String getHandiCap() {
        return handiCap;
    }

    public void setHandiCap(String handiCap) {
        this.handiCap = handiCap;
    }

    public String getRounds() {
        return rounds;
    }

    public void setRounds(String rounds) {
        this.rounds = rounds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getWins() {
        return wins;
    }

    public void setWins(String wins) {
        this.wins = wins;
    }
}

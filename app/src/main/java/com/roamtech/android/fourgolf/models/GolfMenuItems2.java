package com.roamtech.android.fourgolf.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.innodroid.expandablerecycler.ExpandableRecyclerAdapter;

import java.util.List;

/**
 * Created by dennis on 2/9/16.
 */
public class GolfMenuItems2 implements Parcelable{

        public int type;
        public String text;
        public boolean isLoaded;
        public List<GolfMenuItems2> invisibleChildren;

        public GolfMenuItems2() {
        }

        public GolfMenuItems2(int type, String text,boolean isLoaded) {
            this.type = type;
            this.text = text;
            this.isLoaded = isLoaded;
        }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}

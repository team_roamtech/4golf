package com.roamtech.android.fourgolf;

import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.innodroid.expandablerecycler.ExpandableRecyclerAdapter;
import com.roamtech.android.fourgolf.adapters.ExpandableListAdapter;
import com.roamtech.android.fourgolf.adapters.MenuAdapter;
import com.roamtech.android.fourgolf.fragments.ForgotPasswordFragment;
import com.roamtech.android.fourgolf.fragments.GolfClubFragment;
import com.roamtech.android.fourgolf.fragments.LoginFragment;
import com.roamtech.android.fourgolf.fragments.LoginStateFragment;
import com.roamtech.android.fourgolf.fragments.RegistrationFragment;
import com.roamtech.android.fourgolf.interfaces.LaunchFragment;
import com.roamtech.android.fourgolf.interfaces.ScrollToPager;
import com.roamtech.android.fourgolf.models.GolfMenuItems;
import com.roamtech.android.fourgolf.models.GolfMenuItems2;
import com.roamtech.android.fourgolf.utils.CommonTasks;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LaunchFragment,ScrollToPager {
    public static final String TAG = "MainActivity";
    public static final String POPUP_POSITION = "popup_position";
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    LinearLayout lytDrawer;
    private RecyclerView listRecycler;
    MenuAdapter adapter;
    GolfClubFragment golfClubFragment;
    List<GolfMenuItems> golfMenuItemses;
    List<GolfMenuItems2> golfMenuItemses2;
    ExpandableListAdapter exp;
    LoginStateFragment loginStateFragment;
    ForgotPasswordFragment forgotPasswordFragment;
    RegistrationFragment registrationFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        golfMenuItemses= new ArrayList<GolfMenuItems>();
        golfMenuItemses2= new ArrayList<GolfMenuItems2>();
        golfMenuItemses2 = getMenuItems2();
        golfMenuItemses = getMenuItems();

        adapter = new MenuAdapter(this,golfMenuItemses);
        exp = new ExpandableListAdapter(this,golfMenuItemses2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        lytDrawer = (LinearLayout) findViewById(R.id.lyt_drawer);
        listRecycler = (RecyclerView) findViewById(R.id.lst_menu);
        listRecycler.setLayoutManager(new LinearLayoutManager(this));
        listRecycler.setAdapter(exp);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, // nav drawer open - description for accessibility
                R.string.drawer_close // nav drawer close - description for accessibility
        ){
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_add).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == R.id.action_add){
            View menuItemView = findViewById(R.id.action_add);
            PopupMenu popup = new PopupMenu(MainActivity.this, menuItemView);
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.pop_up_me, popup.getMenu());

            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    Bundle args = new Bundle();
                    switch(item.getItemId()){
                        case R.id.login:
                            args.putInt(POPUP_POSITION,R.id.login);
                            loginStateFragment = new LoginStateFragment();
                            loginStateFragment.setArguments(args);
                            CommonTasks.addFragment(loginStateFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) loginStateFragment).getClass().getName());
                            break;
                        case R.id.register:
                            registrationFragment = new RegistrationFragment();
                            CommonTasks.addFragment(registrationFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) registrationFragment).getClass().getName());
                            break;
                        case R.id.forgot_password:
                             args.putInt(POPUP_POSITION,R.id.forgot_password);
                            loginStateFragment = new LoginStateFragment();
                            loginStateFragment.setArguments(args);
                            CommonTasks.addFragment(loginStateFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) loginStateFragment).getClass().getName());
                            break;
                    }

                    return true;
                }
            });

            popup.show();//showing popup menu

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void launchFragment(int position, Bundle bundle, int view) {
        if(view == 0) {
                golfMenuItemses2.get(position).isLoaded = false;
                exp.notifyDataSetChanged();
        }else if(view == 1){

            GolfMenuItems2 gmItems = bundle.getParcelable(ExpandableListAdapter.GOLF_ITEM);
            Log.d(TAG,"position == "+golfMenuItemses2.indexOf(gmItems));
            if (gmItems.text.equals("Golf Clubs")) {
                golfClubFragment = new GolfClubFragment();
                CommonTasks.addFragment(golfClubFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) golfClubFragment).getClass().getName());
                mDrawerLayout.closeDrawer(lytDrawer);
            }
        }
    }

    private List<GolfMenuItems> getMenuItems() {
        List<GolfMenuItems> items = new ArrayList<>();

        items.add(new GolfMenuItems(MenuAdapter.TYPE_HEADER2,"Kenya Golf",false));
        items.add(new GolfMenuItems("KGU",""));
        items.add(new GolfMenuItems("KLGU", ""));
        items.add(new GolfMenuItems("Junior Golf", ""));
        items.add(new GolfMenuItems("Event & Tournaments",""));
        items.add(new GolfMenuItems("Pro Golfers", ""));
        items.add(new GolfMenuItems(MenuAdapter.TYPE_HEADER2,"Golf Clubs",false));
        items.add(new GolfMenuItems(MenuAdapter.TYPE_HEADER2,"My Golf",false));
        items.add(new GolfMenuItems("Personal details", ""));
        items.add(new GolfMenuItems("My Golf Clubs", ""));
        items.add(new GolfMenuItems("Activity", ""));
        items.add(new GolfMenuItems(MenuAdapter.TYPE_HEADER2,"International Golf",false));
        items.add(new GolfMenuItems("Players",""));
        items.add(new GolfMenuItems("PGA", ""));
        items.add(new GolfMenuItems(MenuAdapter.TYPE_HEADER2,"Golf Rules",false));
        items.add(new GolfMenuItems("General rules", ""));
        items.add(new GolfMenuItems("Match Play", ""));
        items.add(new GolfMenuItems("Stroke Play", ""));
        items.add(new GolfMenuItems("Q&A", ""));
        items.add(new GolfMenuItems(MenuAdapter.TYPE_HEADER2,"Kamare",false));

        return items;
    }

    private List<GolfMenuItems2> getMenuItems2() {
        List<GolfMenuItems2> items = new ArrayList<>();

        GolfMenuItems2 kenyaGolf = new GolfMenuItems2(ExpandableListAdapter.HEADER, "Kenya Golf",false);
        kenyaGolf.invisibleChildren = new ArrayList<>();

        kenyaGolf.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "KGU",false));
        kenyaGolf.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "KLGU",false));
        kenyaGolf.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "Junior Golf",false));
        kenyaGolf.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "Event & Tournaments",false));
        kenyaGolf.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "Pro Golfers",false));

        items.add(kenyaGolf);

        GolfMenuItems2 golfClubs = new GolfMenuItems2(ExpandableListAdapter.HEADER, "Golf Clubs",false);
        items.add(golfClubs);

        GolfMenuItems2 myGolf = new GolfMenuItems2(ExpandableListAdapter.HEADER, "My Golf",false);
        myGolf.invisibleChildren = new ArrayList<>();

        myGolf.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "Personal details",false));
        myGolf.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "My Golf Clubs",false));
        myGolf.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "Activity",false));

        items.add(myGolf);

        GolfMenuItems2 intGolf = new GolfMenuItems2(ExpandableListAdapter.HEADER, "International Golf",false);
        intGolf.invisibleChildren = new ArrayList<>();

        intGolf.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "Players",false));
        intGolf.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "PGA",false));

        items.add(intGolf);

        GolfMenuItems2 golfRules = new GolfMenuItems2(ExpandableListAdapter.HEADER, "Golf Rules",false);
        golfRules.invisibleChildren = new ArrayList<>();

        golfRules.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "General rules",false));
        golfRules.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "Match Play",false));
        golfRules.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "Stroke Play",false));
        golfRules.invisibleChildren.add(new GolfMenuItems2(ExpandableListAdapter.CHILD, "Q&A",false));

        items.add(golfRules);

        GolfMenuItems2 kamare = new GolfMenuItems2(ExpandableListAdapter.HEADER, "Kamare",false);
        items.add(kamare);

        return items;
    }

    @Override
    public void scrollToPager(int position) {
        LoginStateFragment lsFrag = (LoginStateFragment)
                getSupportFragmentManager().findFragmentByTag(((Object) loginStateFragment).getClass().getName());

        if (lsFrag != null) {
            // If article frag is available, we're in two-pane layout...

            // Call a method in the ArticleFragment to update its content
            lsFrag.scrollToPager(1);
        }

    }
}

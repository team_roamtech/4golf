package com.roamtech.android.fourgolf.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.roamtech.android.fourgolf.R;
import com.roamtech.android.fourgolf.adapters.ItemPagerAdapter;
import com.roamtech.android.fourgolf.interfaces.LaunchFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends Fragment implements LaunchFragment{
    public static final String TAG = "RegistrationFragment";
    ViewPager viewPager;
    AppCompatActivity mActivity;
    ImageView imgBack;

    public RegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_registration, container, false);
        imgBack = (ImageView) rootView.findViewById(R.id.img_background);



        Glide.with(this)
                .load(R.drawable.golf_back)
                .centerCrop()
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_photo_black_48dp)
                .error(R.drawable.ic_broken_image_black_48dp)
                .into(imgBack);



        viewPager = (ViewPager) rootView.findViewById(R.id.vp_register_overview);
        viewPager.setAdapter(new ItemPagerAdapter(mActivity,this));
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

    }

    @Override
    public void launchFragment(int position, Bundle bundle, int view) {
        if(position == 0 & bundle == null){
            viewPager.setCurrentItem(1);
        }
    }
}

package com.roamtech.android.fourgolf.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.roamtech.android.fourgolf.MainActivity;
import com.roamtech.android.fourgolf.R;
import com.roamtech.android.fourgolf.adapters.LoginPagerAdapter;
import com.roamtech.android.fourgolf.interfaces.ScrollToPager;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginStateFragment extends Fragment {
    ViewPager viewPager;
    AppCompatActivity mActivity;
    ImageView imgBack;
    int pagerPosition;


    public LoginStateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Bundle args = getArguments();
        if(args != null){
            pagerPosition = args.getInt(MainActivity.POPUP_POSITION);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_login_state, container, false);
        imgBack = (ImageView) rootView.findViewById(R.id.img_background);
        Glide.with(this)
                .load(R.drawable.golf_back)
                .centerCrop()
                .thumbnail(0.1f)
                .placeholder(R.drawable.ic_photo_black_48dp)
                .error(R.drawable.ic_broken_image_black_48dp)
                .into(imgBack);

        viewPager = (ViewPager) rootView.findViewById(R.id.vp_login_overview);
        viewPager.setAdapter(new LoginPagerAdapter(mActivity.getSupportFragmentManager(),mActivity));
        if(pagerPosition == R.id.login){
            viewPager.setCurrentItem(0);
        }else if(pagerPosition == R.id.forgot_password){
            viewPager.setCurrentItem(1);
        }

        return rootView;
    }

    public void scrollToPager(int position) {
        viewPager.setCurrentItem(position);
    }
}

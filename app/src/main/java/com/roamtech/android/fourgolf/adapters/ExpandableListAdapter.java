package com.roamtech.android.fourgolf.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.roamtech.android.fourgolf.R;
import com.roamtech.android.fourgolf.interfaces.LaunchFragment;
import com.roamtech.android.fourgolf.models.GolfMenuItems;
import com.roamtech.android.fourgolf.models.GolfMenuItems2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anandbose on 09/06/15.
 */
public class ExpandableListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int HEADER = 0;
    public static final int CHILD = 1;
    public static final String GOLF_ITEM = "golf_item";
    int prevMenuPos = -1;
    GolfMenuItems2 prevItem = null;
    private List<GolfMenuItems2> data;
    private static final int ARROW_ROTATION_DURATION = 150;
    Context ctx;
    LaunchFragment mCallBack;

    public ExpandableListAdapter(Context ctx,List<GolfMenuItems2> data) {
        this.data = data;
        try {
            mCallBack = (LaunchFragment) ctx;
        } catch (ClassCastException e) {
            throw new ClassCastException(ctx.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = null;
        Context context = parent.getContext();
        float dp = context.getResources().getDisplayMetrics().density;
        int subItemPaddingLeft = (int) (18 * dp);
        int subItemPaddingTopAndBottom = (int) (5 * dp);
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (type) {
            case HEADER:
                view = inflater.inflate(R.layout.item_header, parent, false);
                ListHeaderViewHolder header = new ListHeaderViewHolder(view);
                return header;
            case CHILD:
                 inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.item_menu, parent, false);
                ListMenuViewHolder menu = new ListMenuViewHolder(view);
                return menu;
        }
        return null;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final GolfMenuItems2 item = data.get(position);
        switch (item.type) {
            case HEADER:
                final ListHeaderViewHolder itemController = (ListHeaderViewHolder) holder;
                itemController.refferalItem = item;
                itemController.header_title.setText(item.text);
                itemController.btn_expand_toggle.setImageResource(R.drawable.ic_keyboard_arrow_right_black_36dp);
                if (item.isLoaded == true) {
                    //itemController.btn_expand_toggle.setImageResource(R.drawable.circle_minus);
                    openArrow(itemController.btn_expand_toggle);
                } else {
                   // itemController.btn_expand_toggle.setImageResource(R.drawable.circle_plus);
                    closeArrow(itemController.btn_expand_toggle);
                }
                itemController.btn_expand_toggle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemControll(item,itemController);
                    }
                });
                itemController.header_title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemControll(item,itemController);
                    }
                });
                break;
            case CHILD:
                final ListMenuViewHolder itemController2 = (ListMenuViewHolder) holder;
                itemController2.menu_title.setText(item.text);
                //TextView itemTextView = (TextView) holder.itemView;
                //itemTextView.setText(data.get(position).Text);
                break;
        }
    }

    public void itemControll(GolfMenuItems2 item,ListHeaderViewHolder itemController){
        int pos = data.indexOf(itemController.refferalItem);;
        if (item.isLoaded == true) {
            item.invisibleChildren = new ArrayList<GolfMenuItems2>();
            int count = removeItems(pos, data, item);
            notifyItemRangeRemoved(pos + 1, count);
            //itemController.btn_expand_toggle.setImageResource(R.drawable.circle_plus);
            closeArrow(itemController.btn_expand_toggle);
            item.isLoaded = false;
        } else {
            if(item.invisibleChildren != null) {
               // pos = data.indexOf(itemController.refferalItem);
                int index = addItems(pos + 1, data, item);
                notifyItemRangeInserted(pos + 1, index - pos - 1);
                //itemController.btn_expand_toggle.setImageResource(R.drawable.circle_minus);
                openArrow(itemController.btn_expand_toggle);
                item.invisibleChildren = null;
                item.isLoaded = true;
                if(prevItem != null & prevItem != item){
                    // int prevMenuPos = data.indexOf(itemController.refferalItem);
                    prevMenuPos = data.indexOf(prevItem);
                    Log.d("TAG", "previous menu position is " + prevMenuPos);
                    final GolfMenuItems2 item2 = data.get(prevMenuPos);
                    item2.invisibleChildren = new ArrayList<GolfMenuItems2>();
                    int count = removeItems(prevMenuPos,data,item2);
                    notifyItemRangeRemoved(prevMenuPos + 1, count);
                    mCallBack.launchFragment(prevMenuPos, null, 0);
                }
                prevItem = item;
            }
        }
        Bundle bundle = new Bundle();
        bundle.putParcelable(GOLF_ITEM,item);
        mCallBack.launchFragment(pos, bundle, 1);
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).type;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public int addItems(int index,List<GolfMenuItems2> data,GolfMenuItems2 item){
        for (GolfMenuItems2 i : item.invisibleChildren) {
            data.add(index, i);
            index++;
        }
        return index;
    }

    public int removeItems(int pos,List<GolfMenuItems2> data,GolfMenuItems2 item){
        int count = 0;
        while (data.size() > pos + 1 && data.get(pos + 1).type == CHILD) {
            item.invisibleChildren.add(data.remove(pos + 1));
            count++;
        }
        return count;
    }

    private static class ListHeaderViewHolder extends RecyclerView.ViewHolder {
        public TextView header_title;
        public ImageView btn_expand_toggle;
        public GolfMenuItems2 refferalItem;

        public ListHeaderViewHolder(View itemView) {
            super(itemView);
            header_title = (TextView) itemView.findViewById(R.id.txt_header);
            btn_expand_toggle = (ImageView) itemView.findViewById(R.id.item_arrow);
        }
    }

    private static class ListMenuViewHolder extends RecyclerView.ViewHolder {
        public TextView menu_title;

        public ListMenuViewHolder(View itemView) {
            super(itemView);
            menu_title = (TextView) itemView.findViewById(R.id.txt_menu);
        }
    }



    public static void openArrow(View view) {
        view.animate().setDuration(ARROW_ROTATION_DURATION).rotation(90);
    }

    public static void closeArrow(View view) {
        view.animate().setDuration(ARROW_ROTATION_DURATION).rotation(0);
    }
}

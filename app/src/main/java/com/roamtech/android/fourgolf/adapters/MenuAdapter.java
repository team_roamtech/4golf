package com.roamtech.android.fourgolf.adapters;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.innodroid.expandablerecycler.ExpandableRecyclerAdapter;
import com.roamtech.android.fourgolf.R;
import com.roamtech.android.fourgolf.interfaces.LaunchFragment;
import com.roamtech.android.fourgolf.models.GolfMenuItems;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dennis on 2/2/16.
 */
public class MenuAdapter  extends ExpandableRecyclerAdapter<GolfMenuItems> {
    public static final String TAG = "MenuAdapter";
    public static final int TYPE_MENU = 1001;
    public static final int TYPE_HEADER2 = TYPE_HEADER;
    Context ctx;
    int prevMenuPos = -1;
    int prevMenuPos2;
    int currentPos;
    List<GolfMenuItems> golfMenuItems;


    public MenuAdapter(Context context,List<GolfMenuItems> golfMenuItems) {
        super(context);
        this.ctx = context;
        this.golfMenuItems = golfMenuItems;
        setItems(golfMenuItems);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_HEADER2:
                return new GolfHeaderHolder(inflate(R.layout.item_header, parent));
            case TYPE_MENU:
            default:
                return new GolfMenuHolder(inflate(R.layout.item_menu, parent));
        }
    }

    @Override
    public void onBindViewHolder(ExpandableRecyclerAdapter.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_HEADER2:
                ((GolfHeaderHolder) holder).bind(position);
                break;
            case TYPE_MENU:
            default:
                ((GolfMenuHolder) holder).bind(position);

                break;
        }

    }

    public class GolfHeaderHolder extends ExpandableRecyclerAdapter.HeaderViewHolder implements View.OnClickListener {
        TextView name;
        LaunchFragment mCallBack;

        public GolfHeaderHolder(View view) {
            super(view, (ImageView) view.findViewById(R.id.item_arrow));
            try {
                mCallBack = (LaunchFragment) ctx;
            } catch (ClassCastException e) {
                throw new ClassCastException(ctx.toString()
                        + " must implement OnHeadlineSelectedListener");
            }
            name = (TextView) view.findViewById(R.id.txt_header);
        }

        @Override
        public void toggleExpandedItems(int position) {
            currentPos = golfMenuItems.indexOf(visibleItems.get(position));
            super.toggleExpandedItems(position);
            Log.d(TAG, " CurrentPos result " + currentPos + " PrevPos result " + prevMenuPos);
            if(prevMenuPos != -1 & prevMenuPos != currentPos){
                Log.d(TAG, " PrevPos result " + prevMenuPos);
                prevMenuPos2 = visibleItems.indexOf(golfMenuItems.get(prevMenuPos));
                for(int i = 0; i < visibleItems.size(); i++){
                    Log.d(TAG, " visible items " + visibleItems.get(i).Text);
                }
                Log.d(TAG, " PrevPos2 result " + prevMenuPos2);

               if(isExpanded(prevMenuPos2)){
                    collapseItems(prevMenuPos2);
                }
            }


/*            if(golfMenuItems.indexOf(visibleItems.get(position)) == 6 | golfMenuItems.indexOf(visibleItems.get(position)) == 19) {
                int pos = golfMenuItems.indexOf(visibleItems.get(position));
                mCallBack.launchFragment(pos, null, 0);
            }else {*/
//                int pos = golfMenuItems.indexOf(visibleItems.get(position));
  //              mCallBack.launchFragment(pos, null, 0);
           // }
            //prevMenuPos = visibleItems.indexOf(golfMenuItems.get(currentPos));
            prevMenuPos = currentPos;

        }

        Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                Log.d(TAG," boolean result "+golfMenuItems.indexOf(visibleItems.get(currentPos)));
                /*if (isExpanded(0)) {
                    collapseItems(0);
                }*/
            }
        };


        @Override
        public int expandItems(int position){
            return super.expandItems(position);
        }

        @Override
        public void collapseItems(int position) {
            super.collapseItems(position);
        }

        @Override
        public void bind(int position) {
            name.setText(visibleItems.get(position).Text);
            /*if(visibleItems.get(position).isLoaded()){
                handler.post(r);
            }*/
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.txt_header:

                    break;
            }
        }
    }

    public class GolfMenuHolder extends ExpandableRecyclerAdapter.ViewHolder {
        TextView name;

        public GolfMenuHolder(View view) {
            super(view);


            name = (TextView) view.findViewById(R.id.txt_menu);
        }

        public void bind(int position) {
            name.setText(visibleItems.get(position).Text);
        }
    }

}

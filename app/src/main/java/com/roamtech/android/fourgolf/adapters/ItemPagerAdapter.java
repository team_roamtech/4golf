package com.roamtech.android.fourgolf.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.roamtech.android.fourgolf.R;
import com.roamtech.android.fourgolf.interfaces.LaunchFragment;
import com.thomashaertel.widget.MultiSpinner;

public class ItemPagerAdapter extends PagerAdapter {
    public static final String TAG = "ItemPagerAdapter";
    Context ctx;
    private LayoutInflater vi;
    LaunchFragment mCallBack;
    Fragment fragment;
    private ArrayAdapter<String> adapter;
    String [] golfClubs;

    public ItemPagerAdapter(Context ctx,Fragment fragment){
        this.ctx = ctx;
        this.fragment = fragment;
        vi = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            mCallBack = (LaunchFragment) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement LaunchFragment");
        }
        golfClubs = ctx.getResources().getStringArray(R.array.golf_clubs);
        adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_item);
        for(int i = 0; i < golfClubs.length; i++){
            Log.d(TAG,golfClubs[i]);
            adapter.add(golfClubs[i]);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object == view;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        Log.d(TAG, "product items instantiateItem");
        // Inflate a new layout from our resources
        View view = null;
        if(position == 0) {
            view = vi.inflate(R.layout.layout_reg1,
                    container, false);
            view.setTag(position);
            Button btnNext = (Button) view.findViewById(R.id.btn_next);
            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallBack.launchFragment(position,null,0);

                }
            });
            container.addView(view);
        }else if(position == 1){

            view = vi.inflate(R.layout.layout_reg2,
                    container, false);
            view.setTag(position);
            MultiSpinner spinner;
            spinner = (MultiSpinner) view.findViewById(R.id.spinnerMulti);

            spinner.setAdapter(adapter, false, onSelectedListener);
            boolean[] selectedItems = new boolean[adapter.getCount()];
            selectedItems[0] = true; // select second item
            spinner.setSelected(selectedItems);
            container.addView(view);
        }

        return view;
    }

    /**
     * Destroy the item from the {@link android.support.v4.view.ViewPager}. In our case this is simply removing the
     * {@link View}.
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    private MultiSpinner.MultiSpinnerListener onSelectedListener = new MultiSpinner.MultiSpinnerListener() {

        public void onItemsSelected(boolean[] selected) {
            // Do something here with the selected items

            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    builder.append(adapter.getItem(i)).append(" ");
                }
            }

            //Toast.makeText(MainActivity.this, builder.toString(), Toast.LENGTH_SHORT).show();
        }
    };
}
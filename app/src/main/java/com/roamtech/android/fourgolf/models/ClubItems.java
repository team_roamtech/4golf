package com.roamtech.android.fourgolf.models;

/**
 * Created by dennis on 2/3/16.
 */
public class ClubItems {

    public int clubId;
    public String clubName;
    public String clubContacts;
    public String clubLocation;
    public boolean isExpanded;

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setIsExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }

    public int getClubId() {
        return clubId;
    }

    public void setClubId(int clubId) {
        this.clubId = clubId;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getClubContacts() {
        return clubContacts;
    }

    public void setClubContacts(String clubContacts) {
        this.clubContacts = clubContacts;
    }

    public String getClubLocation() {
        return clubLocation;
    }

    public void setClubLocation(String clubLocation) {
        this.clubLocation = clubLocation;
    }
}

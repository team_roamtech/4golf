package com.roamtech.android.fourgolf.models;

import com.innodroid.expandablerecycler.ExpandableRecyclerAdapter;

/**
 * Created by dennis on 2/9/16.
 */
public class GolfMenuItems extends ExpandableRecyclerAdapter.ListItem{
    public String Text;
    public static final int TYPE_MENU = 1001;
    int TYPE_HEADER;
    boolean isLoaded;

    public boolean isLoaded() {
        return isLoaded;
    }

    public void setIsLoaded(boolean isLoaded) {
        this.isLoaded = isLoaded;
    }




    public GolfMenuItems(int TYPE_HEADER,String group,boolean isExpanded) {
        super(TYPE_HEADER);

        Text = group;
    }

    public GolfMenuItems(String menu, String last) {
        super(TYPE_MENU);

        Text = menu;
    }
}

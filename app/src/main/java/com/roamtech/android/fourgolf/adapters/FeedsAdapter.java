package com.roamtech.android.fourgolf.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.roamtech.android.fourgolf.R;
import com.roamtech.android.fourgolf.models.FeedItems;
import com.roamtech.android.fourgolf.models.UserItems;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dennis on 3/15/16.
 */
public class FeedsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    ArrayList<FeedItems> feedItemses;
    UserItems userItems;
    Context ctx;

    public FeedsAdapter(Context ctx,ArrayList<FeedItems> feedItemses,UserItems userItems)
    {
        this.feedItemses = feedItemses;
        this.userItems = userItems;
        this.ctx = ctx;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_HEADER)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_user_header, parent, false);
            return  new VHHeader(v);
        }
        else if(viewType == TYPE_ITEM)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_feed_item, parent, false);
            return new VHItem(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof VHHeader){
            VHHeader VHheader = (VHHeader)holder;
            VHheader.txtName.setText(userItems.getName());
            VHheader.txtEmail.setText(userItems.getEmail());
            VHheader.txtBalance.setText(userItems.getBalance());
            VHheader.txtWin.setText(userItems.getWins());
            VHheader.txtHandicap.setText(userItems.getHandiCap());
            VHheader.txtRounds.setText(userItems.getRounds());
            Glide.with(ctx)
                    .load(R.drawable.sports_golf)
                    .centerCrop()
                    .thumbnail(0.1f)
                    .placeholder(R.drawable.ic_photo_black_48dp)
                    .error(R.drawable.ic_broken_image_black_48dp)
                    .into(VHheader.imgProfilePic);
        }else if(holder instanceof VHItem){
            FeedItems currentItem = getItem(position-1);
            VHItem VHitem = (VHItem)holder;
            VHitem.txtFeeds.setText(currentItem.getFeedText());
        }
    }

    private FeedItems getItem(int position)
    {
        return feedItemses.get(position);
    }

    @Override
    public int getItemCount() {
        return feedItemses.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if(isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }


    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    class VHHeader extends RecyclerView.ViewHolder{
        ImageView imgProfilePic;
        TextView txtName,txtEmail,txtBalance,txtWin,txtHandicap,txtRounds;
        Button btnTopUp;
        ImageButton btnSync;

        public VHHeader(View itemView) {
            super(itemView);
            this.imgProfilePic = (ImageView) itemView.findViewById(R.id.img_profile_pic);
            this.txtName = (TextView) itemView.findViewById(R.id.txt_name);
            this.txtEmail = (TextView) itemView.findViewById(R.id.txt_email);
            this.txtBalance = (TextView) itemView.findViewById(R.id.txt_balance);
            this.txtWin = (TextView) itemView.findViewById(R.id.txt_win);
            this.txtHandicap = (TextView) itemView.findViewById(R.id.txt_handicap);
            this.txtRounds = (TextView) itemView.findViewById(R.id.txt_rounds);
            this.btnTopUp = (Button) itemView.findViewById(R.id.btn_top_up);
            this.btnSync = (ImageButton) itemView.findViewById(R.id.btn_sync);
        }
    }

    class VHItem extends RecyclerView.ViewHolder{
        TextView txtFeeds;
        public VHItem(View itemView) {
            super(itemView);
            this.txtFeeds = (TextView)itemView.findViewById(R.id.txt_feeds);
        }
    }
}

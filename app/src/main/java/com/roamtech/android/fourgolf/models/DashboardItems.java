package com.roamtech.android.fourgolf.models;

/**
 * Created by dennis on 3/15/16.
 */
public class DashboardItems {
    public String strItem;
    public int itemImage;

    public int getItemImage() {
        return itemImage;
    }

    public void setItemImage(int itemImage) {
        this.itemImage = itemImage;
    }

    public String getStrItem() {
        return strItem;
    }

    public void setStrItem(String strItem) {
        this.strItem = strItem;
    }


}
package com.roamtech.android.fourgolf.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.roamtech.android.fourgolf.fragments.ForgotPasswordFragment;
import com.roamtech.android.fourgolf.fragments.LoginFragment;

/**
 * Created by dennis on 3/2/16.
 */
public class LoginPagerAdapter extends FragmentStatePagerAdapter {
    Context ctx;
    LoginFragment loginFragment;
    ForgotPasswordFragment forgotPasswordFragment;

    public LoginPagerAdapter(FragmentManager fm,Context ctx) {
        super(fm);
        this.ctx = ctx;

    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            loginFragment = new LoginFragment();
            return loginFragment;
        }else if(position == 1){
            forgotPasswordFragment = new ForgotPasswordFragment();
            return forgotPasswordFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Parcelable saveState(){
        return null;
    }
}

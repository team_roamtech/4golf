package com.roamtech.android.fourgolf.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roamtech.android.fourgolf.R;
import com.roamtech.android.fourgolf.interfaces.LaunchFragment;
import com.roamtech.android.fourgolf.models.ClubItems;

import java.util.ArrayList;

/**
 * Created by dennis on 2/3/16.
 */
public class ClubsAdapter extends RecyclerView.Adapter<ClubsAdapter.ClubHolder>{
    public static final String TAG = "ClubsAdapter";
    public static final String ISEXPANDED = "isexpanded";
    ArrayList<ClubItems> clubItemses = new ArrayList<>();
    Context ctx;
    Fragment fragment;

    public ClubsAdapter(Context ctx,ArrayList<ClubItems> clubItemses,Fragment fragment){
        this.clubItemses = clubItemses;
        this.ctx = ctx;
        this.fragment = fragment;

    }

    @Override
    public ClubHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_club_item, parent, false);
        ClubHolder clubHolder = new ClubHolder(v,fragment);
        return clubHolder;
    }

    @Override
    public void onBindViewHolder(ClubHolder holder, int position) {
        holder.txtGolfClub.setText(clubItemses.get(position).getClubName());
        holder.txtContact.setText(clubItemses.get(position).getClubContacts());
        holder.txtLocation.setText(clubItemses.get(position).getClubLocation());
        if(clubItemses.get(position).isExpanded == true){
            holder.lytInner.setVisibility(View.VISIBLE);
            holder.imgDown.setImageResource(R.drawable.collapse_arrow);
        }else if(clubItemses.get(position).isExpanded == false){
            holder.lytInner.setVisibility(View.GONE);
            holder.imgDown.setImageResource(R.drawable.expand_arrow);
        }

    }

    @Override
    public int getItemCount() {
        return clubItemses.size();
    }

    public static class ClubHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView imgDown;
        TextView txtGolfClub,txtContact,txtLocation;
        LinearLayout lytInner;
        LinearLayout lytCard;
        LaunchFragment mCallBack;
        Fragment fragment;

        public ClubHolder(View itemView,Fragment fragment) {
            super(itemView);
            this.fragment = fragment;
            try {
                mCallBack = (LaunchFragment) fragment;
            } catch (ClassCastException e) {
                throw new ClassCastException(fragment.toString()
                        + " must implement OnHeadlineSelectedListener");
            }
            txtGolfClub = (TextView)itemView.findViewById(R.id.txt_golf_club);
            txtContact = (TextView)itemView.findViewById(R.id.txt_contact);
            txtLocation = (TextView)itemView.findViewById(R.id.txt_location);
            lytInner = (LinearLayout)itemView.findViewById(R.id.lyt_inner_elements);
            lytCard = (LinearLayout)itemView.findViewById(R.id.lyt_card);
            lytCard.setOnClickListener(this);
            imgDown = (ImageView)itemView.findViewById(R.id.img_down);
            if(lytInner.getVisibility() == View.VISIBLE) {
                imgDown.setImageResource(R.drawable.collapse_arrow);
            }else{
                imgDown.setImageResource(R.drawable.expand_arrow);
            }
            imgDown.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.img_down:
                    Log.d(TAG, "position is " + getAdapterPosition());
                    Bundle args = new Bundle();
                    if(lytInner.getVisibility() == View.GONE) {
                        args.putBoolean(ClubsAdapter.ISEXPANDED,true);
                    //    lytInner.setVisibility(View.VISIBLE);
                      //  imgDown.setImageResource(R.drawable.collapse_arrow);
                    }else if(lytInner.getVisibility() == View.VISIBLE){
                        args.putBoolean(ClubsAdapter.ISEXPANDED,false);
                     //   lytInner.setVisibility(View.GONE);
                       // imgDown.setImageResource(R.drawable.expand_arrow);
                    }

                    mCallBack.launchFragment(getAdapterPosition(),args,R.id.img_down);
                    break;
                case R.id.lyt_card:
                    mCallBack.launchFragment(getAdapterPosition(),null,R.id.lyt_card);
                    break;
                default:

                    break;
            }
        }

    }
}

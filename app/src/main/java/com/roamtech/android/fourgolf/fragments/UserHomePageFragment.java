package com.roamtech.android.fourgolf.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.roamtech.android.fourgolf.R;
import com.roamtech.android.fourgolf.adapters.FeedsAdapter;
import com.roamtech.android.fourgolf.models.FeedItems;
import com.roamtech.android.fourgolf.models.UserItems;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserHomePageFragment extends Fragment {
    AppCompatActivity mActivity;
    Context ctx;
    RecyclerView feedsRecycler;
    ArrayList<FeedItems> feedItemsArrayList = new ArrayList<FeedItems>();
    FeedsAdapter feedsAdapter;
    UserItems userItems;


    public UserHomePageFragment() {
        // Required empty public constructor
    }

    public void onAttach(Context context){
        super.onAttach(context);
        this.ctx = context;
        mActivity = (AppCompatActivity)ctx;
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_user_home_page, container, false);

        feedsRecycler = (RecyclerView) rootView.findViewById(R.id.lst_feeds);
        String [] feeds = mActivity.getResources().getStringArray(R.array.feeds);
        userItems = new UserItems();
        userItems.setName(" GEORGE NJOROGE ");
        userItems.setEmail("themburu@gmail.com");
        userItems.setBalance("Shs " + "23450");
        userItems.setWins("450");
        userItems.setHandiCap("40");
        userItems.setRounds("40");
        returnFeeds(feeds);
        LinearLayoutManager llm = new LinearLayoutManager(mActivity);
        feedsRecycler.setLayoutManager(llm);
        feedsAdapter = new FeedsAdapter(mActivity,feedItemsArrayList,userItems);
        feedsRecycler.setAdapter(feedsAdapter);

        return rootView;
    }

    private  ArrayList<FeedItems> returnFeeds(String [] feeds){

        for(int i = 0; i < feeds.length; i++){
            FeedItems feedItems = new FeedItems();
            feedItems.setFeedText(feeds[i]);

            feedItemsArrayList.add(feedItems);
        }
        return feedItemsArrayList;
    }

}

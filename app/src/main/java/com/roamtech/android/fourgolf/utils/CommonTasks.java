package com.roamtech.android.fourgolf.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by dennis on 2/3/16.
 */
public class CommonTasks {

    public static void addFragment(Fragment fragment, boolean addToBackStack, int transition,int layoutResourceID, FragmentManager fm, String tag){
        FragmentTransaction ft = fm.beginTransaction();
        //if(fm.findFragmentByTag(tag) == null) {
        //if (!fragmentPopped) {
        ft.replace(layoutResourceID, fragment, tag);
        //}
        ft.setTransition(transition);
        if (addToBackStack)
            ft.addToBackStack(tag);
        ft.commit();
       /* }else{
            Log.d(TAG, " boolean fragment  already Added ");
            boolean fragmentPopped = fm.popBackStackImmediate(tag,p);
            //ft.show(fragment).commit();

        }*/
    }
}

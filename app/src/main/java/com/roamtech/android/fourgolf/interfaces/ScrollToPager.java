package com.roamtech.android.fourgolf.interfaces;

/**
 * Created by dennis on 3/9/16.
 */
public interface ScrollToPager {
    public void scrollToPager(int position);
}

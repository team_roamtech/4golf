package com.roamtech.android.fourgolf.fragments;


import android.app.FragmentTransaction;
import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.roamtech.android.fourgolf.R;
import com.roamtech.android.fourgolf.interfaces.ScrollToPager;
import com.roamtech.android.fourgolf.utils.CommonTasks;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener{
    TextView txtRegister,txtForgotPassword;
    Button btnLogin;
    AppCompatActivity mActivity;
    RegistrationFragment registrationFragment;
    UserHomePageFragment userHomePageFragment;
    ScrollToPager mCallBack;


    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
        try {
            mCallBack = (ScrollToPager) mActivity;
        } catch (ClassCastException e) {
            throw new ClassCastException(mActivity.toString()
                    + " must implement interface");
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
       setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        txtRegister = (TextView) rootView.findViewById(R.id.txt_register);
        txtRegister.setText(Html.fromHtml(mActivity.getResources().getString(R.string.txt_new_user)));
        txtRegister.setOnClickListener(this);

        txtForgotPassword = (TextView) rootView.findViewById(R.id.txt_forgot_password);
        txtForgotPassword.setText(Html.fromHtml(mActivity.getResources().getString(R.string.txt_forgot_password)));
        txtForgotPassword.setOnClickListener(this);

        btnLogin = (Button) rootView.findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.txt_register:
                registrationFragment = new RegistrationFragment();
                CommonTasks.addFragment(registrationFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) registrationFragment).getClass().getName());
                break;
            case R.id.txt_forgot_password:
                mCallBack.scrollToPager(1);
                break;
            case R.id.btn_login:
                userHomePageFragment = new UserHomePageFragment();
                CommonTasks.addFragment(userHomePageFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) userHomePageFragment).getClass().getName());
                break;
            default:
                break;
        }
    }
}

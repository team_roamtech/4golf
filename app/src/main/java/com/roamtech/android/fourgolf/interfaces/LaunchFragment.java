package com.roamtech.android.fourgolf.interfaces;

import android.os.Bundle;
import android.view.View;

/**
 * Created by dennis on 2/23/15.
 */
public interface LaunchFragment {
    public void launchFragment(int position, Bundle bundle, int view);
}

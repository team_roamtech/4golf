package com.roamtech.android.fourgolf.fragments;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.roamtech.android.fourgolf.R;
import com.roamtech.android.fourgolf.adapters.ClubsAdapter;
import com.roamtech.android.fourgolf.interfaces.LaunchFragment;
import com.roamtech.android.fourgolf.models.ClubItems;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class GolfClubFragment extends Fragment implements LaunchFragment {
    AppCompatActivity mActivity;
    RecyclerView clubsRecycler;
    ProgressBar progressBar;
    ClubsAdapter adapter;
    ArrayList<ClubItems> clubItemses = new ArrayList<ClubItems>();
    String [] golfClubs;


    public GolfClubFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        golfClubs = mActivity.getResources().getStringArray(R.array.golf_clubs);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_golf_club, container, false);
        progressBar = (ProgressBar) rootView.findViewById(R.id.list_progress);
        clubsRecycler = (RecyclerView) rootView.findViewById(R.id.lst_golf_clubs);
        LinearLayoutManager llm = new LinearLayoutManager(mActivity);
        clubsRecycler.setLayoutManager(llm);
        clubItemses = golfClubses(golfClubs);
        adapter = new ClubsAdapter(mActivity,clubItemses,this);
        clubsRecycler.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);

        return rootView;
    }

    public ArrayList<ClubItems> golfClubses(String [] golfClubs){
        ArrayList<ClubItems> clubItemses = new ArrayList<ClubItems>();
        for(int i = 0; i < golfClubs.length; i++){
            ClubItems clubItems = new ClubItems();
            clubItems.setClubName(golfClubs[i]);
            clubItems.setClubContacts("0722123456 0733123456");
            clubItems.setClubLocation("Nairobi Upperhill");
            clubItemses.add(clubItems);
        }

        return clubItemses;
    }


    @Override
    public void launchFragment(int position, Bundle bundle, int view) {
        switch(view){
            case R.id.img_down:
                clubItemses.get(position).setIsExpanded(bundle.getBoolean(ClubsAdapter.ISEXPANDED,false));
                adapter.notifyDataSetChanged();
                break;
            case R.id.lyt_card:
                String url = "http://www.google.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
        }
    }
}
